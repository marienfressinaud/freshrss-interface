<?php

class InterfaceExtension extends Minz_Extension {
	public function init() {
		Minz_View::appendStyle($this->getFileUrl('style.css', 'css'));
		$this->registerViews();
	}
}
